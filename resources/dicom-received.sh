#!/bin/sh

source_dir="$( cd "$( dirname "${0}" )" &> /dev/null && pwd )"

[[ -z $1 ]] && exit 1
dicom_dir="$1"

echo "[RECEIVER] Received experiment stored in ${dicom_dir}"
echo "$@"
### Get experiment information from DICOM headers ###
first_dicom=$(find ${dicom_dir} -type f -name '*dcm' |head -1) 
# echo "[RECEIVER] ${dicom_dir} found first dicom ${first_dicom}"

patient_id=`dcmdump +P "0010,0020" ${first_dicom}| cut -d'[' -f2 | cut -d']' -f1`
accession_nr=$(dcmdump +P "0008,0050" ${first_dicom}| cut -d'[' -f2 | cut -d']' -f1)
timestamp=$(date "+%Y%m%d_%H%M")
echo "[RECEIVER][${dicom_dir}] Received at ${timestamp}: "
echo "[RECEIVER][${dicom_dir}] Patient ID:  ${patient_id}"
echo "[RECEIVER][${dicom_dir}] Accession Nr: ${accession_nr}"
###     Can be deleted/commented if not needed    ###


### Put your own code to process DICOM ###

##########################################

# delete tmp folder
echo "[RECEIVER] Deleting data from  ${dicom_dir}"
#rm -r "${dicom_dir}"
