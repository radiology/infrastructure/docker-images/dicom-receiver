#!/bin/sh

export DCMDICTPATH=/usr/local/share/dcmtk/dicom.dic

[[ -z $RECEIVED_SCRIPT ]] && echo "Missing RECEIVED_SCRIPT environment variable" && exit
[[ -z $AETITLE ]] && export AETITLE="STORESCP"
[[ -z $DICOM_TEMP ]] && mkdir -p /tmp/dicom_receiver/ && export DICOM_TEMP=/tmp/dicom_receiver/
[[ -z $PORT ]] &&  export PORT=8105

storescp \
    -xcs "${RECEIVED_SCRIPT} #p" \
    -tos 15 \
    -fe .dcm \
    -su suid \
    +xa \
    --aetitle ${AETITLE} \
    --output-directory ${DICOM_TEMP} \
    --disable-host-lookup \
    $PORT 2>&1 |tee  /var/log/storescp.log 