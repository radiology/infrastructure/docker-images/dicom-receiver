# Dicom Receiver

Dicom receiver based on dcmtk's storescp installed in an Python image.

# Additional dependencies
 Additional dependencies should be installed using a derived image.
 
# Configuration
The following environment vars vould be set
```BASH
    RECEIVED_SCRIPT: required - Location to shell script to be executed upon experiment received.
    AETITLE: optional - default value is STORESCP
    DICOM_TEMP: optional default value is /tmp/dicom_receiver/ 
    PORT: optional default value is 8105
```

# Automation
Added gitlab CI pipline, to automated docker image building.
Release should be build automatically when being push with COMMIT_TAG
Develop should be build automatically every commit/push.

# Changelog

0.1.2 - 2023-04-21
------------------
* Update to dcmtk version 3.6.7
* Added deployment charts
* Added studyclient + xnatpy to default image for conveniences.

0.1.1 - 2021-03-24
------------------

Bugfix: Fix correct name of dicomreceiver.sh  script in image.
0.1.0 - 2021-03-23
------------------

* First version
* Automation