FROM python:3.10.8-alpine3.16

ENV DCMTK_VERSION 3.6.7

# Install build  tools
RUN apk update && \
    apk add --no-cache libstdc++ && \
    apk add --no-cache --virtual .build-deps  g++ make git cmake

# Clone repository
RUN git clone https://github.com/DCMTK/dcmtk.git

# Configure  dcmtk
WORKDIR /dcmtk
RUN git checkout DCMTK-${DCMTK_VERSION}
RUN cmake .

# Build dcmtk
RUN make all && \
    make install && \
    make clean

# Remove Source
RUN cd .. && \
    rm -r dcmtk

# Delete build dependencies
RUN apk del .build-deps && \
    rm /var/cache/apk/*

# Add studyclient and xnatpy for conveniences
RUN pip install xnat
RUN pip install https://gitlab.com/radiology/infrastructure/study-governor-client/-/archive/feature/api-v2/study-governor-client-feature-api-v2.zip

# Add dicom-receiver scripts
RUN mkdir -p /opt/dicom-receiver
ADD ./scripts/dicom-receiver.sh /opt/dicom-receiver
CMD /opt/dicom-receiver/dicom-receiver.sh
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "" ]
