.PHONY:  help up down


REGISTRY=registry.localhost:15000
all: up

up:
	$(MAKE) -C .k8s/k3d setup

down:
	$(MAKE) -C .k8s/k3d teardown

#Helm install name

# List of helm targets
HELM_TARGETS = all
# Add all helm targets with the "helm-" prefix.
help:
	@echo '*** Usage of this file:'
	@echo 'make           : Setup local dev environment using k3d (see .k3s/k3d for configs) AND build and push all local images'
	@echo 'make up        : Setup local dev environmetn (k8s cluster and registry)'
	@echo 'make down      : Teardown local dev environment'
	@echo
	@echo '*** Some helm related commands:'